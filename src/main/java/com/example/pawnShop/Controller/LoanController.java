package com.example.pawnShop.Controller;

import com.example.pawnShop.Dto.LoanRequest;
import com.example.pawnShop.Dto.LoanResponse;
import com.example.pawnShop.Service.PawnShopServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Validated
@RestController
@RequiredArgsConstructor

public class LoanController {

    private final PawnShopServiceImpl service;

    @PostMapping("/loans")
    public ResponseEntity<LoanResponse> handleCreateNewLoan (@RequestBody LoanRequest body) {

        return new ResponseEntity<>(service.createNewLoan(body), HttpStatus.OK);

    }

}
