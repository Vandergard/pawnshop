package com.example.pawnShop.Service;

import com.example.pawnShop.Dto.LoanRequest;
import com.example.pawnShop.Dto.LoanResponse;
import com.example.pawnShop.Dto.PawnConditions;
import com.example.pawnShop.Entity.LoanEntity;
import com.example.pawnShop.Entity.LoanPartEntity;
import com.example.pawnShop.Entity.PawnEntity;
import com.example.pawnShop.Entity.PersonEntity;
import com.example.pawnShop.Exception.LoanAlreadyExistException;
import com.example.pawnShop.Repository.LoanPartRepository;
import com.example.pawnShop.Repository.LoanRepository;
import com.example.pawnShop.Repository.PawnRepository;
import com.example.pawnShop.Repository.PersonRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class PawnShopServiceImpl {

    private final PawnRepository pawnEntityRepository;
    private final PersonRepository personEntityRepository;
    private final LoanRepository loanRepository;
    private final LoanPartRepository loanPartRepository;


    PawnShopServiceImpl(PawnRepository pawnEntityRepository, PersonRepository personEntityRepository, LoanRepository loanRepository, LoanPartRepository loanPartRepository){
        this.pawnEntityRepository = pawnEntityRepository;
        this.personEntityRepository = personEntityRepository;
        this.loanRepository = loanRepository;
        this.loanPartRepository = loanPartRepository;

    }

    @Transactional
    public LoanResponse createNewLoan (LoanRequest body) throws RuntimeException {

        LoanResponse loanResponse = new LoanResponse();

        if ((loanRepository.findByPawnShopTicketId(body.getPawnshopTicketId()))!=null){
            throw new LoanAlreadyExistException(body.getPawnshopTicketId());
        }
        else {
            loanResponse.setPawnshopTicketId(body.getPawnshopTicketId());
            PersonEntity pledgerPerson=personEntityRepository.findByFullNameAndDocumentSerno(body.getFullName(), body.getDocumentSerno());
            if (pledgerPerson==null){
                pledgerPerson = new PersonEntity(body.getFullName(), body.getDocumentSerno(),body.getDocumentAddress(), body.getDocumentBirthday(), body.getPhoneNumber());
            }
            else {
                if(pledgerPerson.getDocumentAddress()!=body.getConfidantDocumentAddress()){
                    pledgerPerson.setDocumentAddress(body.getDocumentAddress());
                }
                if (pledgerPerson.getPhoneNumber()!=body.getPhoneNumber()){
                    pledgerPerson.setPhoneNumber(body.getPhoneNumber());
                }
            }
            PersonEntity persistPersonEntity = personEntityRepository.save(pledgerPerson);

            loanResponse.setFullName(persistPersonEntity.getFullName());
            loanResponse.setDocumentSerno(persistPersonEntity.getDocumentSerno());
            loanResponse.setDocumentAddress(persistPersonEntity.getDocumentAddress());
            loanResponse.setDocumentBirthday(persistPersonEntity.getDocumentBirthday());
            loanResponse.setPhoneNumber(persistPersonEntity.getPhoneNumber());

            LoanEntity loanEntity = new LoanEntity(body.getPawnshopTicketId(), "CREATED", body.getDateReturn());
            loanEntity.setPledgerPerson(persistPersonEntity);
            loanEntity.setConfidantFullName(body.getConfidantFullName());
            loanEntity.setConfidantPhoneNumber(body.getConfidantPhoneNumber());
            loanEntity.setConfidantDocumentSerno(body.getConfidantDocumentSerno());
            loanEntity.setConfiantDocumentAddress(body.getConfidantDocumentAddress());
            loanEntity.setConfidantDocumentBirthday(body.getConfidantDocumentBirthday());

            List <LoanPartEntity> loanPartEntityList = new ArrayList<>();
            List <PawnEntity> pawnEntityList = new ArrayList<>();
            for (PawnConditions pawnCondition : body.getPawnConditionsList() ){
                PawnEntity pawnEntity = new PawnEntity(
                        pawnCondition.getPawnName(), pawnCondition.getProductWeight(), pawnCondition.getGoldContent(), pawnCondition.getAdditionalInfo());
                pawnEntityList.add(pawnEntity);
                LoanPartEntity loanPartEntity= new LoanPartEntity(
                        loanEntity, pawnEntity, pawnCondition.getLoanAmount(), pawnCondition.getAssessmentAmount(),
                        pawnCondition.getLoanPercent(), pawnCondition.getLatenessPercent(), pawnCondition.getTaxPercent(), "CREATED");
                loanPartEntityList.add(loanPartEntity);
            }

            loanEntity.setLoanPartEntityList(loanPartEntityList);
            LoanEntity loanPersistEntity = loanRepository.save(loanEntity);

            loanResponse.setConfidantFullName(loanPersistEntity.getConfidantFullName());
            loanResponse.setConfidantDocumentSerno(loanPersistEntity.getConfidantDocumentSerno());
            loanResponse.setConfidantDocumentAddress((loanPersistEntity.getConfiantDocumentAddress()));
            loanResponse.setConfidantDocumentBirthday(loanPersistEntity.getConfidantDocumentBirthday());
            loanResponse.setConfidantPhoneNumber(loanPersistEntity.getConfidantPhoneNumber());
            loanResponse.setLoanId(loanEntity.getId());
        }


        return loanResponse;
    }

}
