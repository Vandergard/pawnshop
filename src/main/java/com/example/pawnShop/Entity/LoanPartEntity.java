package com.example.pawnShop.Entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "loan_part_entity")
@NoArgsConstructor
@ToString(exclude = {"id"})
public class LoanPartEntity {

    public LoanPartEntity (LoanEntity loan, PawnEntity pawn, BigDecimal loanPartAmount, BigDecimal assessmentAmount, int loanPartPercent, int latePercent, int taxPercent, String status){
        this.loan = loan;
        this.pawn = pawn;
        this.loanPartAmount = loanPartAmount;
        this.assessmentAmount = assessmentAmount;
        this.loanPartPercent = loanPartPercent;
        this.latePercent = latePercent;
        this.taxPercent = taxPercent;
        this.status = status;
    }

    @Getter
    @Id
    @GeneratedValue
    private Long id;

    @Getter
    @Setter
    @ManyToOne
    private LoanEntity loan;

    @Getter
    @Setter
    @OneToOne (cascade = CascadeType.PERSIST)
    @JoinColumn(name="pawn_id", nullable=false)
    private PawnEntity pawn;

    @Getter
    @Setter
    private BigDecimal loanPartAmount;

    @Getter
    @Setter
    private BigDecimal assessmentAmount;

    @Getter
    @Setter
    private int loanPartPercent;

    @Getter
    @Setter
    private int latePercent;

    @Getter
    @Setter
    private int taxPercent;

    @Getter
    @Setter
    private String status;

}
