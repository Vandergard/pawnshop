package com.example.pawnShop.Entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "person_entity")
@NoArgsConstructor
@ToString(exclude = {"id", "pledgeFullName", "birthday", "address"})
public class PersonEntity {

    public PersonEntity(String fullName, String documentSerno, String documentAddress, String documentBirthday, String phoneNumber){
        this.fullName = fullName;
        this.documentSerno = documentSerno;
        this.documentAddress = documentAddress;
        this.documentBirthday = documentBirthday;
        this.phoneNumber = phoneNumber;
    }

    public PersonEntity(String fullName, String phoneNumber){
        this.fullName = fullName;
        this.phoneNumber = phoneNumber;
    }

    public PersonEntity(String fullName, String documentSerno, String documentAddress, String documentBirthday){
        this.fullName = fullName;
        this.documentSerno = documentSerno;
        this.documentAddress = documentAddress;
        this.phoneNumber = documentBirthday;
    }

    @Getter
    @Id @GeneratedValue
    private  Long id;

    @Getter
    @Setter
    private String fullName;

    @Getter
    @Setter
    private String documentSerno;

    @Getter
    @Setter
    private String documentAddress;

    @Getter
    @Setter
    private String documentBirthday;  //it should be Date format in DB

    @Getter
    @Setter
    private String phoneNumber;

    @Getter
    @Setter
    @ManyToMany
    private List<LoanEntity> loans;

}
