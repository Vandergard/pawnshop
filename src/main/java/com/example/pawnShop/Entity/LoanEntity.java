package com.example.pawnShop.Entity;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "loan_entity")
@NoArgsConstructor
@ToString(exclude = {"id"})
public class LoanEntity {

    public LoanEntity(Long pawnShopTicketId, String status, String dateReturn){
        this.pawnShopTicketId = pawnShopTicketId;
        this.status = status;
        this.dateReturn = dateReturn;
    }

    @Getter
    @Id
    @GeneratedValue
    private Long id;

    @Getter
    @Setter
    @Column(unique = true)
    @NotNull
    private Long pawnShopTicketId;

    @Getter
    @Setter
    private String status;

    @Getter
    @Setter
    private String dateReturn;

    @Getter
    @Setter
    @OneToMany(mappedBy = "loan", cascade=CascadeType.ALL)
    List<LoanPartEntity> loanPartEntityList;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name="person_id", nullable=false)
    private PersonEntity pledgerPerson;


    @Getter
    @Setter
    private String confidantFullName;

    @Getter
    @Setter
    private String confidantDocumentSerno;

    @Getter
    @Setter
    private String confiantDocumentAddress;

    @Getter
    @Setter
    private String confidantDocumentBirthday;  //it should be Date format in DB

    @Getter
    @Setter
    private String confidantPhoneNumber;

}
