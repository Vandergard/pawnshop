package com.example.pawnShop.Entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "pawn_entity")
@NoArgsConstructor
@ToString(exclude = {"id", "productType", "productWeight", "goldContent"})
public class PawnEntity {

    public PawnEntity (String pawnName, BigDecimal productWeight, int goldContent, String status){
        this.pawnName = pawnName;
        this.productWeight = productWeight;
        this.goldContent = goldContent;
        this.status = status;
    }

    @Getter
    @Id
    @GeneratedValue
    private Long id;


    @Getter
    @Setter
    private String pawnName;

    @Getter
    @Setter
    private BigDecimal productWeight;

    @Setter
    @Getter
    private int goldContent;

    @Getter
    @Setter
    private String additionalInformation;

    @Getter
    @Setter
    private String status;

    @Getter
    @Setter
    private BigDecimal productSalesAmount;

    @Getter
    @Setter
    @OneToOne
    @JoinColumn(name = "loan_part_id")
    private LoanPartEntity associatedLoanPart;

}
