package com.example.pawnShop.Repository;

import com.example.pawnShop.Entity.LoanPartEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LoanPartRepository extends JpaRepository<LoanPartEntity, Long> {
}
