package com.example.pawnShop.Repository;

import com.example.pawnShop.Entity.LoanEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LoanRepository extends JpaRepository<LoanEntity, Long> {

    LoanEntity findByPawnShopTicketId (Long pawnShopTicketId);

}
