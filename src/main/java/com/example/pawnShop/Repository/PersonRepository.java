package com.example.pawnShop.Repository;

import com.example.pawnShop.Entity.PersonEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<PersonEntity, Long> {

    PersonEntity findByFullNameAndDocumentSerno(String fullName, String documentSerno);
}
