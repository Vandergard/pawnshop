package com.example.pawnShop.Repository;

import com.example.pawnShop.Entity.PawnEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PawnRepository  extends JpaRepository<PawnEntity, Long> {
}
