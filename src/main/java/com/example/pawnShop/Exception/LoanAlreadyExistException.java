package com.example.pawnShop.Exception;

public class LoanAlreadyExistException extends RuntimeException {
    public LoanAlreadyExistException(Long pawnShopTicketId){
        super("Loan with this pawnShopTicketId " + pawnShopTicketId + " is already exist");
    }
}
