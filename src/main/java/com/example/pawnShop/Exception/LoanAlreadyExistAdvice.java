package com.example.pawnShop.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;


@ControllerAdvice
public class LoanAlreadyExistAdvice {

    @ResponseBody
    @ExceptionHandler(LoanAlreadyExistException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    String loanAlreadyExistHandler (LoanAlreadyExistException ex){
        return ex.getMessage();
    }
}
