package com.example.pawnShop;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EntityScan("com.example.pawnShop.Entity")
public class LoadDatabase {
    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);


//    @Bean
//    CommandLineRunner iniPersontDatabase(PersonRepository repository) {
//        return args -> {
//            log.info("Preloading " + repository.save(new PersonEntity("Ivanov Ivan Ivanovich", "MP111920", "Bishkek ")));
//        };
//    }

}
