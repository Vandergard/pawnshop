package com.example.pawnShop.Dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import java.util.List;

@Entity
@Data
public class LoanResponse {

    @Getter
    @Setter
    private Long loanId;

    @Getter
    @Setter
    private String dateCreate;

    @Getter
    @Setter
    private String dateRefresh;

    @Getter
    @Setter
    private Long pawnshopTicketId;

    @Getter
    @Setter
    private String dateReturn;

    @Getter
    @Setter
    private String loanTerm;

    //Pledger Person
    @Getter
    @Setter
    private String fullName;

    @Getter
    @Setter
    private String documentSerno;

    @Getter
    @Setter
    private String documentAddress;

    @Getter
    @Setter
    private String documentBirthday;

    @Getter
    @Setter
    private String phoneNumber;

    @Getter
    @Setter
    private String confidantFullName;

    @Getter
    @Setter
    private String confidantDocumentSerno;

    @Getter
    @Setter
    private String confidantDocumentAddress;

    @Getter
    @Setter
    private String confidantDocumentBirthday;

    @Getter
    @Setter
    private String confidantPhoneNumber;

    @Getter
    @Setter
    private List<PawnInformations> pawnsInformation;

}
