package com.example.pawnShop.Dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

public class PawnConditions {

    //Pawn description
    @Getter
    @Setter
    private String pawnName;

    @Getter
    @Setter
    private BigDecimal productWeight;

    @Getter
    @Setter
    private int goldContent;

    @Getter
    @Setter
    private String additionalInfo;

    //Conditions
    @Getter
    @Setter
    private BigDecimal loanAmount;

    @Getter
    @Setter
    private BigDecimal assessmentAmount;

    @Getter
    @Setter
    private  int loanPercent;

    @Getter
    @Setter
    private BigDecimal loanPercentAmount;

    @Getter
    @Setter
    private int latenessPercent;

    @Getter
    @Setter
    private int taxPercent;
}
