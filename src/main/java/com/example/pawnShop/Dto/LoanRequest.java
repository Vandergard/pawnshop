package com.example.pawnShop.Dto;


import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class LoanRequest {

    //Pledger information
    @Getter
    @Setter
    private String fullName;

    @Getter
    @Setter
    private String documentSerno;

    @Getter
    @Setter
    private String documentAddress;

    @Getter
    @Setter
    private String documentBirthday;

    @Getter
    @Setter
    private String phoneNumber;

    //Confidant-person information
    @Getter
    @Setter
    private String confidantFullName;

    @Getter
    @Setter
    private String confidantPhoneNumber;

    @Getter
    @Setter
    private String confidantDocumentSerno;

    @Getter
    @Setter
    private String confidantDocumentAddress;

    @Getter
    @Setter
    private String confidantDocumentBirthday;


    //Pledge information
    // It could be list of collateral items

    @Getter
    @Setter
    private Long pawnshopTicketId;

    @Getter
    @Setter
    private String dateReturn;

    @Getter
    @Setter
    private String loanTerm;

    @Getter
    @Setter
    private List<PawnConditions> pawnConditionsList;

}
